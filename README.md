# ceo

## Requirements 

* Ant


## Installation

```
$ git clone git@gitlab.com:ucsdlibrary/development/ceo.git
```

## Usage

1.Open project.

```
$ cd ceo
```

2.Build ceo.zip

```
$ ant clean webapp
```

## Deploying with GitLab ChatOps
This application is setup currently for deployments to be done via GitLab's
chatops feature.

The supported environments to deploy to are:
- staging
- production

Example: `/gitlab ucsdlibrary/development/ceo run production_deploy`

By default, it will deploy the specified default branch, which is
`master`. If you would like to specify a different branch or tag, you can pass
that as an argument to the slack command.

Example: `/gitlab ucsdlibrary/development/ceo run production_deploy 1.5.0`

The above command will deploy the `1.5.0` tag to `production`.

### Scheduling A Production Deployment
The application also supports setting up a GitLab [Scheduled
Pipeline][gitlab-schedule] to support production deployments during the
Operations service window (6-8am PST).

An example schedule using [Cron syntax][cron] might be `30 06 * * 2` which would deploy
the application at 06:30AM on Monday.

Make sure you set the `Cron timezone` in the schedule for `Pacific Time (US and
Canada)`

After the deploy, make sure you de-active or delete the Schedule.
Otherwise the application may accidentally deploy again the following week.

[cron]:https://en.wikipedia.org/wiki/Cron
[gitlab-schedule]:https://docs.gitlab.com/ee/ci/pipelines/schedules.html

